/**
 * LF2Player class
 * 
 * C# code: Ayalx
 * C++ code: MangaD
 * Copyright: Ayalx
 */

#pragma once

#include <string>
#include <cstdint>//uint8_t
#include <atomic>
#include <chrono>
#include "socket.hpp"

class LF2Player {
	public:
		std::atomic<bool> isActive;
		uint8_t outCount;
		sock::Socket clientSocket;
		int playerIndex;
		uint8_t lastFInput1;
		uint8_t lastFInput2;
		uint8_t lastKeyInput;
		std::atomic<std::chrono::duration<int,std::milli>> totalWaitAmount;
		std::string playerName;

		LF2Player(sock::Socket socket_conn)
			: isActive(true), outCount(6), clientSocket(std::move(socket_conn)), playerIndex(0),
			  lastFInput1(0), lastFInput2(0), lastKeyInput(0), totalWaitAmount(std::chrono::milliseconds(0)),
			  playerName()
		{}

		//move constructor
		LF2Player(LF2Player &&rhs)
			: isActive(rhs.isActive.load()), outCount(rhs.outCount), clientSocket(std::move(rhs.clientSocket)),
			  playerIndex(rhs.playerIndex), lastFInput1(rhs.lastFInput1), lastFInput2(rhs.lastFInput2),
			  lastKeyInput(rhs.lastKeyInput), totalWaitAmount(rhs.totalWaitAmount.load()), playerName(rhs.playerName)
		{}

		//Names have underscores after them?
		void setPlayerName(std::string name)
		{
			int length = 0;
			for(int i = static_cast<int>(name.length()) - 1; i >= 0; i--) {
				if(name[i] != '_')
				{
					length = i + 1;
					break;
				}
			}
			if(length != 0)
				playerName = name.substr(0, length);
			else
				playerName = "NoName";
		}

		bool operator==(const LF2Player &rhs) const {
			if(playerIndex == rhs.playerIndex) return true;
			else return false;
		}
};
