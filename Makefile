# Project: LF2 Multi Server v2.0
# Makefile created by MangaD

# Notes:
#  - If 'cmd /C' does not work, replace with 'cmd //C'

CMD = cmd /C

CXX       ?= g++
SRC_FOLDER = src
OBJ_FOLDER = build
RES_FOLDER = res
LIB_FOLDER = lib
INC_FOLDER = include
BIN_FOLDER = bin/gnu_
ifeq ($(OS),Windows_NT)
BIN_FOLDER = bin\gnu_
endif
# By default, we build for release
BUILD=release

SUBDIRS = $(LIB_FOLDER)
ifeq ($(OS),Windows_NT)
RES_FOLDER := $(RES_FOLDER)/windows
SUBDIRS += $(RES_FOLDER)
else
RES_FOLDER := $(RES_FOLDER)/linux
endif

# SRC is a list of the cpp files in the source directory.
SRC = $(wildcard $(SRC_FOLDER)/*.cpp)
# OBJ is a list of the object files to be generated in the objects directory.
OBJ = $(subst $(SRC_FOLDER),$(OBJ_FOLDER), $(SRC:.cpp=.o))
RES = $(wildcard $(RES_FOLDER)/*.rc)

RES_OBJ = $(RES:.rc=.o)

ifeq ($(OS),Windows_NT)
OBJ += $(RES_OBJ)
endif

DEFINES=

ifneq ($(BUILD),release)
WARNINGS = -Wall -Wextra -pedantic -Wmain -Weffc++ -Wswitch-default \
	-Wswitch-enum -Wmissing-include-dirs -Wmissing-declarations -Wunreachable-code -Winline \
	-Wfloat-equal -Wundef -Wcast-align -Wredundant-decls -Winit-self -Wshadow -Wnon-virtual-dtor \
	-Wconversion
ifeq ($(CXX),g++)
WARNINGS += -Wzero-as-null-pointer-constant -Wuseless-cast
else
ifeq ($(CXX),clang++)
#http://clang.llvm.org/docs/ThreadSafetyAnalysis.html
WARNINGS += -Wthread-safety
endif
endif
endif

# Debug or Release #
ifeq ($(BUILD),release)
# "Release" build - optimization, no debug symbols and no assertions (if they exist)
OPTIMIZE    = -O3 -s -DNDEBUG
BIN_FOLDER := $(BIN_FOLDER)release
STATIC      = -static -static-libgcc -static-libstdc++
else
# "Debug" build - no optimization, with debugging symbols and assertions (if they exist), and sanitize
OPTIMIZE    = -O0 -g -DDEBUG -fsanitize=undefined -fsanitize=thread
BIN_FOLDER := $(BIN_FOLDER)debug
endif

# ARCHITECTURE (x64 or x86) #
# Do NOT use spaces in the filename nor special characters
ifeq ($(ARCH),x64)
BIN          = $(BIN_FOLDER)/MultiServer_x64
ARCHITECTURE = -m64
else ifeq ($(ARCH),x86)
BIN          = $(BIN_FOLDER)/MultiServer_x86
ARCHITECTURE = -m32
else
BIN          = $(BIN_FOLDER)/MultiServer
endif

ifeq ($(OS),Windows_NT)
BIN := $(BIN).exe
endif

# INCLUDES #
INCS = -isystem $(INC_FOLDER)

# LIBRARIES #
LDFLAGS = -L$(LIB_FOLDER)
LDLIBS += -pthread -lsocket -lclipboard
ifeq ($(OS),Windows_NT)
LDLIBS += -lws2_32 -liphlpapi $(STATIC)
endif
LDLIBS += $(ARCHITECTURE)

# Add to LDLIBS `-fsanitize=undefined -fsanitize=thread` OR `-fsanitize=memory -fsanitize-memory-track-origins=2` OR `-fsanitize=address`
# when compiling with Clang for testing. Use `-fno-omit-frame-pointer` with any of them. If there are errors at runtime, they will be printed to stderr.
# Note: In Ubuntu I needed to `sudo ln -s /usr/bin/llvm-symbolizer-3.8 /usr/bin/llvm-symbolizer`
# for `-fsanitize=memory -fsanitize-memory-track-origins=2`. But still got false positives, probably needed to:
# http://stackoverflow.com/questions/20617788/using-memory-sanitizer-with-libstdc
ifneq ($(BUILD),release)
LDLIBS += -fsanitize=undefined -fsanitize=thread
endif

CXXFLAGS = $(INCS) $(ARCHITECTURE) -std=c++14 $(DEFINES) $(WARNINGS) $(OPTIMIZE)

all: $(BIN)
.PHONY : all clean run run32 run64 debug release release32 release64 debug32 debug64 $(SUBDIRS) install uninstall

$(BIN): $(OBJ_FOLDER) $(BIN_FOLDER) $(SUBDIRS) $(OBJ)
	$(CXX) $(OBJ) -o $@ $(LDFLAGS) $(LDLIBS)

$(OBJ_FOLDER)/%.o: $(SRC_FOLDER)/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

# DEPENDENCIES #
$(OBJ_FOLDER)/main.o: $(INC_FOLDER)/rlutil.h
$(OBJ_FOLDER)/main.o: $(SRC_FOLDER)/utils.hpp
$(OBJ_FOLDER)/main.o: $(INC_FOLDER)/socket.hpp
$(OBJ_FOLDER)/main.o: $(INC_FOLDER)/clipboard.hpp
$(OBJ_FOLDER)/main.o: $(SRC_FOLDER)/LF2Player.hpp
$(OBJ_FOLDER)/utils.o: $(SRC_FOLDER)/utils.hpp

$(SUBDIRS):
	$(MAKE) -C $@ BUILD=$(BUILD) ARCH=$(ARCH)

debug:
	make BUILD=debug
release:
	make BUILD=release
release32:
	make BUILD=release ARCH=x86
release64:
	make BUILD=release ARCH=x64
debug32:
	make BUILD=debug ARCH=x86
debug64:
	make BUILD=debug ARCH=x64

install: bin/gnu_release/$(shell basename $(BIN))
ifneq ($(OS),Windows_NT)
	mkdir -p /usr/local/bin
	mkdir -p /usr/local/share/applications/
	mkdir -p /usr/local/share/icons/hicolor/32x32/apps
	cp $< /usr/local/bin/$(shell basename $(BIN))
	cp $(RES_FOLDER)/MultiServer.desktop /usr/local/share/applications/
	cp $(RES_FOLDER)/msicon-1.png /usr/local/share/icons/hicolor/32x32/apps/lf2multiserver2.png
endif

uninstall:
ifneq ($(OS),Windows_NT)
	rm -f /usr/local/bin/$(shell basename $(BIN))
	rm -f /usr/local/share/applications/MultiServer.desktop
	rm -f /usr/local/share/icons/hicolor/32x32/apps/lf2multiserver2.png
endif

clean:
ifeq ($(OS),Windows_NT)
	FOR %%D IN ($(SUBDIRS)) DO $(MAKE) -C %%D clean
else
	@$(foreach dir,$(SUBDIRS),$(MAKE) -C $(dir) clean;)
endif
ifeq ($(OS),Windows_NT)
	$(CMD) "del *.o $(SRC_FOLDER)\*.o $(OBJ_FOLDER)\*.o" 2>nul
else
	rm -f *.o $(SRC_FOLDER)/*.o $(OBJ_FOLDER)/*.o
endif

run:
	$(BIN)

run32:
	make "ARCH=x86" run
run64:
	make "ARCH=x64" run

$(OBJ_FOLDER):
ifeq ($(OS),Windows_NT)
	$(CMD) "mkdir $(OBJ_FOLDER)"
else
	mkdir -p $(OBJ_FOLDER)
endif

$(BIN_FOLDER):
ifeq ($(OS),Windows_NT)
	$(CMD) "mkdir $(BIN_FOLDER)"
else
	mkdir -p $(BIN_FOLDER)
endif
