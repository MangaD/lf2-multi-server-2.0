#include "clipboard.hpp"
#include <iostream>
#include <string>
#include <cstring>//memcpy
#ifdef _WIN32
#include <windows.h>
#else
#include <cstdlib>//system
#endif

namespace clip {

#ifdef _WIN32
	std::string ClipboardErrorMessage(int error) {

		if(error == 0)
			return std::string();

		LPSTR buffer = nullptr;
		size_t size;

		if( (size = FormatMessageA(
			    FORMAT_MESSAGE_ALLOCATE_BUFFER | // allocate buffer on local heap for error text
			    FORMAT_MESSAGE_FROM_SYSTEM | // use system message tables to retrieve error text
			    FORMAT_MESSAGE_IGNORE_INSERTS, // Important! will fail otherwise, since we're not
			                                   //(and CANNOT) pass insertion parameters
			    nullptr, // NULL since source is internal message table
			             // unused with FORMAT_MESSAGE_FROM_SYSTEM
			    error,
			    MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),//MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
			    (LPTSTR) &buffer,
			    0, // minimum size for output buffer
			    nullptr ))
		   == 0)
		{
			std::cerr << "Format message failed: " << GetLastError() << std::endl;
			SetLastError(error);
			return std::string();
		}
		std::string errString(buffer, size);
		LocalFree(buffer);
	
		return errString;
	}
#endif

	void setClipboardText(std::string text) {
	#ifdef _WIN32
		HANDLE hData;//For the data to send to the clipboard
		char *ptrData = nullptr;//pointer to allow char copying

		//get handle to memory to hold phrase
		if((hData = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, text.size() + 1)) == nullptr) {
			int error = GetClipboardError();
			throw clipboard_exception(error, ClipboardErrorMessage(error));
		}

		ptrData = (char*)GlobalLock(hData);//get pointer from handle
		
		memcpy(ptrData,text.c_str(),text.size() + 1);//copy over the phrase

		GlobalUnlock(hData);//free the handle
		
		if(!OpenClipboard(nullptr)) {
			int error = GetClipboardError();
			GlobalFree(hData); 
			throw clipboard_exception(error, ClipboardErrorMessage(error));
		}
		if(!EmptyClipboard() || SetClipboardData(CF_TEXT, hData) == nullptr){
			int error = GetClipboardError();
			CloseClipboard();
			GlobalFree(hData); 
			throw clipboard_exception(error, ClipboardErrorMessage(error));
		}
		CloseClipboard();
		GlobalFree(hData);
	#elif defined(__APPLE__)
		throw clipboard_exception(0, "Yet to implement in this OS.");
	#else
		if (!system(nullptr) || system(("echo \"" + text + "\"" + " | xclip -selection clipboard").c_str())) {
			throw clipboard_exception(0, "xclip needs to be installed in order to use this command.");
		}
	#endif
	}

}