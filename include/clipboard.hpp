#pragma once

#include <string>
#include <exception>
#ifdef _WIN32
#include <windows.h>
#else
#include <cstring>//strerror
#endif

namespace clip {
	class clipboard_exception : public std::exception {
		public:
			explicit clipboard_exception(int code, std::string message = "clipboard_exception")
				: std::exception(), error_code(code), error_message(message) {
				error_message += " (" + std::to_string(code) + ")";
			}
			const char *what() const noexcept
			{
				return error_message.c_str();
			}
			int get_error_code() const noexcept
			{
				return error_code;
			}

		private:
			int error_code = 0;
			std::string error_message;
	};
#ifdef _WIN32
	std::string ClipboardErrorMessage(int error);
	inline int GetClipboardError(){ return GetLastError(); }
#else
	inline std::string ClipboardErrorMessage(int error) { 
		return std::strerror(error);
	}
	inline int GetClipboardError(){ return errno; }
#endif
	void setClipboardText(std::string text);
}