/**
 * Utility functions
 * 
 * C# code: Ayalx
 * C++ code: MangaD
 * Copyright: Ayalx
 */

#pragma once

#include <random>//C++11-style random
#include <vector>
#include <string>
#include <sstream>//stringstream
#include <iomanip>//setw

template <typename T>
void nextBytes(std::vector<T> &buffer)
{
	std::random_device seeder;
	std::mt19937 engine(seeder());
	std::uniform_int_distribution<int> distribution =
		std::uniform_int_distribution<int>(0, 255);
	for (auto &i : buffer)
	{
		i = static_cast<T>(distribution(engine));
	}
}

template <typename T>
std::string to_string_with_precision(const T a_value, const int n = 6)
{
	std::stringstream ss;
	ss << std::setw(n) << std::setfill('0') << a_value;
	return ss.str();
}

void print_colored_text(int color, std::string text, bool newline=false);
std::string read_colored_text(int color);
