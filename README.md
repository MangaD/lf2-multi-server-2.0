# LF2 Multi Server v2.0

![icon](http://s6.postimg.cc/3x4zbxgnx/louisex.png)

This repository contains the source code for the LF2 Multi Server v2.0 program necessary 
for the [Little Fighter 2](http://www.littlefighter.com/) network mode game to work with
up to 8 players from different computers. 
Little Fighter 2 is property of Marti Wong & Starsky Wong. This program is meant to improve
the game's network mode by allowing more than 2 computers to connect.
LF2 Multi Server v2.0 belongs to ayalx. The translation [from C#](https://www.lf-empire.de/forum/showthread.php?tid=10277) to C++ was done by MangaD.
This program by no means intends to harm the game or its users, nor make profit or claim
any credits from Marti Wong & Starsky Wong, nor from ayalx. It is purely fan made.

## Dependencies

+ libpthread
+ [libsocket](https://bitbucket.org/MangaD/libsocket) (downloaded directly with git --recursive, no need to install)

## Build

Get source:

`git clone --recursive -j4 https://bitbucket.org/MangaD/lf2-multi-server-2.0`

### Using GNU Make (Windows, Linux, Mac)

#### Install build tools

##### Linux

Debian/Ubuntu/Mint: `$ sudo apt-get install build-essential g++ g++-multilib libpthread-stubs0-dev`

RedHat/CentOS/Fedora: `$ sudo yum install gcc gcc-c++ glibc-devel libgcc libstdc++-devel glibc-devel.i686 libgcc.i686 libstdc++-devel.i686`

##### Windows

###### MinGW Compiler

* Download latest [MinGW](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/)
* Install MinGW
	* Architecture: i686 - for compiling 32 bit programs
	* Architecture: x86_64 - for compiling 64 bit programs, better for 64-bit processors
	* Threads: posix
* Copy "mingw32-make.exe" and rename the copy to "make.exe"
* Put the MinGW bin folder in the path


#### Build

Use `$ make debug` for debug and `$ make release` for release. If compiling for a specific architecture use:

###### Debug 32-bit
`$ make debug32`

###### Debug 64-bit
`$ make debug64`

###### Release 32-bit
`$ make release32`

###### Release 64-bit
`$ make release64`

Note: It is possible to compile with Clang by setting CXX environment variable to clang++. `$ CXX=clang++ make`

### Using cmake (Windows, Linux and Mac)

*CMake 3.2.2 or higher is required.*

#### Install build tools

Debian/Ubuntu/Mint: `$ sudo apt-get install build-essential g++ g++-multilib libpthread-stubs0-dev cmake`

RedHat/CentOS/Fedora: `$ sudo yum install gcc gcc-c++ glibc-devel libgcc libstdc++-devel glibc-devel.i686 libgcc.i686 libstdc++-devel.i686 cmake`

Windows: Download [Cmake](https://cmake.org/download/). You need either 
[MinGW](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/) 
or [Visual Studio](https://www.visualstudio.com/).

#### Build

`$ mkdir build`
`$ cd build`

Generate Unix Makefile `$ cmake ..`

Generate Unix CodeBlocks project: `$ cmake -G "CodeBlocks - Unix Makefiles" ..`

Generate MinGW Makefile: `$ cmake -G "MinGW Makefiles" ..`

Generate MinGW CodeBlocks project: `$ cmake -G "CodeBlocks - MinGW Makefiles" ..`

Generate Visual Studio 2015 project: `$ cmake -G "Visual Studio 14 2015" ..`

Generate Visual Studio 2015 project (64-bit): `$ cmake -G "Visual Studio 14 2015 Win64" ..`

...

## Documentation

Documentation will probably be generated with Doxygen.

## Tests

Tests will probably be made with cppunit.

## External Libraries

* Socket class by MangaD
* [rlutil](https://github.com/tapio/rlutil) by Tapio Vierros. Used for colored text.

## Contribution guidelines

* Original program: Ayalx
* C++ code: MangaD

## Authors

Little Fighter 2 was made by Marti Wong & Starsky Wong.
LF2 Multi Server v2.0 was originally developed in C# by ayalx. This project is a rewrite in
C++ by David Gon�alves (MangaD).

## Copyright and Licensing

Little Fighter 2 is property of Marti Wong & Starsky Wong.
LF2 Multi Server v2.0 is property of ayalx.

## TODO

* Write tests with cppunit
* Improve README file