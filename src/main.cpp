/**
 * Main file
 * 
 * C# code: Ayalx
 * C++ code: MangaD
 * Copyright: Ayalx
 */

#include <iostream>
#include <string>
#include <cstdint>//uint8_t
#include <ctype.h>//tolower
#include <algorithm>//transform, find
#include <random>//shuffle
#include <map>//multimap
#include <sstream>//stringstream
#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <exception>
#include <stdexcept>
#include <atomic>
#include <utility>// std::pair
#include <chrono>
#include "socket.hpp"//Socket must be included before rlutil, for Windows
#include "rlutil.h"
#include "clipboard.hpp"
#include "utils.hpp"
#include "LF2Player.hpp"

#ifdef _WIN32
#define PORT_IN_USE 10048
#else
#define PORT_IN_USE 98
#endif

using namespace std;
using namespace sock;
using namespace rlutil;
using namespace chrono;

vector<LF2Player> players;
int clientsCount;
const int minimum_players_count = 1;
const int minimum_players_count_in_game = 2;
const int maximum_players_count = 8;
const int port = 12345;
const string start_connection_buffer = "u can connect\0"s;//c++14 - need s suffix or string constructor
                                                          //with 14 length specified, so the trailing zero is added
const int players_list_buffer_size = 77;
const int random_data_buffer_size = 3001;
const int random_data_players_names_start = 300;
const int random_data_players_names_gap = 40;
const int normal_buffer_size = 22;
const uint8_t lf2_shift_opcode = 5;
const int commands_input_thread_init_wait = 1000;
atomic<bool> serverActive(false);
atomic<bool> gameLoaded(false);

atomic<bool> pingCommandActive(false);
atomic<size_t> pingPlayerIndex(0);
int pingPlayerLoopsCount = 0;
int pingTotalLoopsCount = 0;
const int ping_command_loops_per_player = 30;
const int ping_lf2_wait_between_frames = 33;

int activePlayersCount = 0;
time_point<system_clock> gameBeginTime;
int playerColors[8] = { LIGHTGREEN, LIGHTRED, LIGHTMAGENTA, LIGHTBLUE,
                        GREEN, RED, MAGENTA, BLUE };
//Thread synch stuff
mutex cmdMutex;
condition_variable cmdCV;
mutex pingMutex;
condition_variable pingCV;

//Function prototypes
void start();
void startServer();
void getClientsCount();
void waitClients();
void sendToAll(string);
void gameInit();
string getPlayerNicknameByIndex(string,int);
void handleGameCycles();
void handleCycle(string&, uint8_t&, uint8_t&);
void handleCyclePingEnabled(string&, uint8_t&, uint8_t&);
void receiveFromPlayer(LF2Player&, string&, duration<int,milli>&);
void receiveFromPlayer(LF2Player &, string &);
void sendToPlayer(LF2Player &, string);
void startCommandsInput();

thread commandsInputThread(startCommandsInput);

int main() {

	rlutil::setConsoleTitle("LF2 Multi Server v2.0");

	SocketInitializer sockInit;//Initializing Winsock (for Windows)

	cout << "Welcome to ";
	print_colored_text(YELLOW, "LF2 Multi Server v2.0!\n", true);

	start();
	
	return 0;
}

void start() {

	try {
		startServer();
	}
	catch (exception &e) {
		print_colored_text(YELLOW, "Error: " + string(e.what()), true);
	}

	players.clear();//close sockets

	if(serverActive) {
		cout << "Game is over. ";

		auto gameDuration = system_clock::now() - gameBeginTime;
		hours h = duration_cast<hours>(gameDuration);
		gameDuration -= h;
		minutes m = duration_cast<minutes>(gameDuration);
		gameDuration -= m;
		seconds s = duration_cast<seconds>(gameDuration);
		string s_duration = to_string(h.count()) + "h " +
		                    to_string(m.count()) + "m " +
		                    to_string(s.count()) + "s";

		print_colored_text(LIGHTMAGENTA, "Game duration: " + s_duration, true);
		serverActive = false;
		pingCV.notify_one();//pingEventFinsihed.Set(); //in case ping command is running
		cout << "\nPress any key to continue..." << endl;
		commandsInputThread.join();
	}
}

void startServer() {

	getClientsCount();
	waitClients();
	gameInit();

}

void getClientsCount() {
	print_colored_text(LIGHTCYAN, "Type players count (between " + to_string(minimum_players_count) +
	                              " to " + to_string(maximum_players_count) +
	                              "), and then press enter to continue.\nPlayers count: ");
	saveDefaultColor();
	while(true) {
		string input = read_colored_text(LIGHTRED);
		stringstream myStream(input);
		if(myStream >> clientsCount && clientsCount >= minimum_players_count && clientsCount <= maximum_players_count) {
			break;
		}
		print_colored_text(YELLOW, "Error: Incorrect players count.", true);
		print_colored_text(LIGHTCYAN, "Players count: ");
	}
}

void waitClients() {

	ServerSocket serverSocket(port);

	while(true) {
		try {
			serverSocket.bind();
			serverSocket.listen();
			break;
		}
		catch (socket_exception &se) {
			if(se.get_error_code() == PORT_IN_USE) {
				cout << "The port " << port << " is already in use. "
				        "If LF2 is currently running, close it and press enter when ready."
				        << endl;
				cin.get();
			}
			else {
				throw se;
			}
		}
	}

	cout << "Server has been activated." << endl;

	while (players.size() < static_cast<size_t>(clientsCount)) {
		cout << "\nWaiting for player number " << players.size()+1 << " to connect..." << endl;
		Socket conn = serverSocket.accept();
		print_colored_text(playerColors[players.size()%8],
		                  "Player number " + to_string(players.size()+1) + " has connected from " +
		                  conn.getRemoteSocketAddress() + ".", true);
		players.emplace_back(move(conn));
	}


	activePlayersCount = clientsCount;

	cout << "\nAll players have connected." << endl;
}

void gameInit() {
	sendToAll(start_connection_buffer);

	//get players names
	string nicknamesList;
	for(size_t i = 0; i < players.size(); i++) {
		players[i].clientSocket.setBufferLength(players_list_buffer_size);
		string playersList = players[i].clientSocket.read();

		//all the players use controls of player 1
		string playerName = getPlayerNicknameByIndex(playersList, 0);

		players[i].setPlayerName(playerName);
		players[i].playerIndex = static_cast<int>(i);

		//4 names are sent by the normal connection string.
		//the 4 others (if exist) are sent in the random buffer.
		if(i < 4) {
			nicknamesList += playerName;
		}
	}
	//preapre nicknames list to be sent to the players, add empty slots for names that are not used.
	//this list contains the first 4 players names.
	for(size_t i = players.size(); i < 4; i++) {
		nicknamesList += "___________";//11 underscores
	}

	nicknamesList.push_back('\0');

	cout << "\nServer has made connections with " << players.size() << " players." << endl;

	for(size_t i = 0; i < players.size(); i++) {
		print_colored_text(playerColors[i%8], "Player number " + to_string(i+1) + ": ");
		cout << players[i].playerName << endl;
	}

	//send for each player which input he is going to use.
	for(size_t i = 0; i < players.size(); i++) {
		string clientPlayersInput = "11111111000000000000000000000000";//Eight 1's, corresponding to player slots
		clientPlayersInput[i] = '0';
		clientPlayersInput += nicknamesList;
		players[i].clientSocket.write(clientPlayersInput);
	}

	//create a random buffer (this buffer will be used to randomize actions in the game)
	vector<uint8_t> randomData(random_data_buffer_size, 0);
	nextBytes(randomData);

	//Ayalx: add the 4 other players names - if exists.
	//lf2 supports sending only 4 player names, so in order to send another 4 names I have to do it somewhere else.
	//so I encode the 4 other names in the Random buffer!
	//
	//to encode the names of the last 4 players, I do the follows:
	//  - jump to byte 300
	//  - write the first name in jumps of one byte ('a',random byte,'b',random byte'...) until 10 bytes are written, or null to indicate end of name.
	//  - jump 40 bytes, and write the next name. and so on.
	//
	//in this way, the buffer is still very haphazard, so it wouldn't be noticed at all.
	for(size_t i = 4; i < players.size(); i++) {
		int start = random_data_players_names_start + (static_cast<int>(i) - 4) *
                    random_data_players_names_gap;

		for(size_t t = 0, s = 0; t < players[i].playerName.size() + 1; t++, s += 2) {
			if(t != players[i].playerName.size()) {
				randomData[start+s] = players[i].playerName[t];
			}
			else {
				randomData[start+s] = 0;//end of name
			}
		}

	}
	//send the random buffer
	string array = string(randomData.begin(), randomData.end());
	sendToAll(array);

	cout << "\nThe game has started!" << endl;

	gameBeginTime = system_clock::now();
	serverActive = true;

	//start a new thread to get commands from the user
	cmdCV.notify_one();

	//start the game!
	handleGameCycles();
}

void sendToAll(string data) {
	for(auto &player : players) {
		player.clientSocket.write(data);
	}
}

string getPlayerNicknameByIndex(string names, int playerIndex)
{
	return names.substr(32 + 11 * playerIndex, 11);
}

void handleGameCycles()
{
	string inputBuffer;
	for(auto &player : players) {
		player.clientSocket.setBufferLength(normal_buffer_size);
    }

	uint8_t playerFInput1;
	uint8_t playerFInput2;

	while(true) {
		//init f keys
		playerFInput1 = 1;
		playerFInput2 = 1;

		if(!pingCommandActive) {
			handleCycle(inputBuffer, playerFInput1, playerFInput2);
		}
		else {
			handleCyclePingEnabled(inputBuffer, playerFInput1, playerFInput2);
		}
	}
}

void handleCycle(string& inputBuffer, uint8_t& playerFInput1, uint8_t& playerFInput2) {
	//receive input from all players
	for(size_t i = 0; i < players.size(); i++) {
		receiveFromPlayer(players[i], inputBuffer);

		//get F keys
		//Still - if 2 users in the very same moment press 2 diffrent F Keys, the game will be disconnected.
		if(players[i].lastFInput1 != 1) {
			playerFInput1 = players[i].lastFInput1;
        }
		if(players[i].lastFInput2 != 1) {
			playerFInput2 = players[i].lastFInput2;
        }
	}
	gameLoaded = true;

	//prepare output with the players move
	for(size_t i = 0; i < players.size(); i++) {
		inputBuffer[i] = players[i].lastKeyInput;
	}
	//add the f keys
	inputBuffer[10] = playerFInput1;
	inputBuffer[12] = playerFInput2;

	//send output to all players
	for(size_t i = 0; i < players.size(); i++) {
		sendToPlayer(players[i], inputBuffer);
	}
}

void handleCyclePingEnabled(string& inputBuffer, uint8_t& playerFInput1, uint8_t& playerFInput2) {
	//ping command is active
	//the idea is: Send() does not take a lot of time, because it just push the data to a buffer and the OS sends it later.
	//so I check only the time Receive() takes.
	//because I want to get the time that took the socket to respond, I have to receive its data first, before the others, and send his data last.

	//if we have already sampled the player 10 times, move to the next player.
	if(pingPlayerLoopsCount > ping_command_loops_per_player) {
		pingPlayerIndex++;
		pingPlayerLoopsCount = 0;
	}

	//if we have already went over on all the players, we finished.
	if (pingPlayerIndex >= players.size()) {
		pingCommandActive = false;
		pingCV.notify_one();//pingEventFinsihed.Set();
		return;
	}

	//if player is not active, continue to the next one
	if (!players[pingPlayerIndex].isActive) {
		pingPlayerIndex++;
		pingPlayerLoopsCount = 0;
		return;
	}

	pingTotalLoopsCount++;
	pingPlayerLoopsCount++;

	//recevie data from the current player
	LF2Player &player = players[pingPlayerIndex];
	duration<int,milli> receiveTime;
	receiveFromPlayer(player, inputBuffer, receiveTime);

	//sum the time. on the first loop, don't sum it because the send order was incorrect.
	if (pingPlayerLoopsCount != 1) {
		player.totalWaitAmount = player.totalWaitAmount.load() + receiveTime;//FIXME No better way to sum?
	}

	if (player.lastFInput1 != 1) {
		playerFInput1 = player.lastFInput1;
	}
	if (player.lastFInput2 != 1) {
		playerFInput2 = player.lastFInput2;
	}

	//receive input from the other players
	for(size_t i = 0; i < players.size(); i++) {
		if (i == pingPlayerIndex) continue;
		receiveFromPlayer(players[i], inputBuffer, receiveTime);
		//get F keys
		//Still - if 2 users in the very same moment press 2 diffrent F Keys, the game will be disconnected.
		if(players[i].lastFInput1 != 1) {
			playerFInput1 = players[i].lastFInput1;
        }
		if(players[i].lastFInput2 != 1) {
			playerFInput2 = players[i].lastFInput2;
        }
	}

	//prepare output with the players move
	for(size_t i = 0; i < players.size(); i++) {
		inputBuffer[i] = players[i].lastKeyInput;
	}
	//add the f keys
	inputBuffer[10] = playerFInput1;
	inputBuffer[12] = playerFInput2;

	//send output to all players - the current pinged player is sent last
	for(size_t i = 0; i < players.size(); i++) {
		if (i == pingPlayerIndex) continue;
		sendToPlayer(players[i], inputBuffer);
	}
	sendToPlayer(player, inputBuffer);
}

/**
 * receives input from player, and measures the time.
 * used by Ping command.
 */
void receiveFromPlayer(LF2Player& player, string& data, duration<int,milli>& receiveTime) {
	auto now = system_clock::now();
	receiveFromPlayer(player, data);
	auto diff = system_clock::now() - now;
	receiveTime = duration_cast<milliseconds>(diff);
}

/**
 * receives input from player.
 * responsible to handle a disconnection of the player.
 */
void receiveFromPlayer(LF2Player &player, string &data){
	if(player.isActive)
	{
		try {
			data = player.clientSocket.read();
		}
		catch (exception &e) {
			print_colored_text(YELLOW, "Error: " + string(e.what()), true);
			cout << "Player number " << player.playerIndex + 1 << ", \"" << player.playerName
			     << "\", has disconnected.\n" << endl;
			player.isActive = false;
			player.lastFInput1 = 1;
			player.lastFInput2 = 1;
			player.lastKeyInput = 1;
		}

		//get key input
		player.lastKeyInput = data[player.playerIndex];

		//get F keys
		//Fkey 1 - index 10
		//Fkey 2 - index 12
		player.lastFInput1 = data[10];
		player.lastFInput2 = data[12];
	}
	else
	{
		if(player.outCount == 0)
			return;
		if (player.outCount == 6) {
			//decrease the number of current online players
			activePlayersCount--;

			try{
				player.clientSocket.close();
			}
			catch (socket_exception &se) {
				print_colored_text(YELLOW, "Error: " + string(se.what()), true);
			}

			if(activePlayersCount < minimum_players_count_in_game) {
				throw runtime_error("All players have been disconnected.");
			}
		}
		player.lastFInput1 = 1;
		player.lastFInput2 = 1;
		player.lastKeyInput = 1;

		//send "jump" from this user to the other clients.
		//if the user was choosing a player when he has suddenly disconnected,
		//it will cancel his choosing so the game will not stuck for the other players.
		if(player.outCount % 2 == 0) {
			player.lastKeyInput = lf2_shift_opcode;
		}
		player.outCount--;
	}
}

/**
 * sends output to player.
 * responsible to handle a disconnection of the player.
 */
void sendToPlayer(LF2Player &player, string data)
{
	if(!player.isActive)
		return;
	try {
		player.clientSocket.write(data);
	}
	catch (exception &e) {
		print_colored_text(YELLOW, "Error: " + string(e.what()), true);
		cout << "Player number " << player.playerIndex + 1 << ", \"" << player.playerName
		     << "\", has disconnected.\n" << endl;
		player.isActive = false;
	}
}

void startCommandsInput() {
	unique_lock<mutex> cm(cmdMutex);
	cmdCV.wait(cm, []{return serverActive.load();});

	this_thread::sleep_for(milliseconds(commands_input_thread_init_wait));
	print_colored_text(LIGHTCYAN, "\nType \"players\" to copy the players list to the clipboard.", true);
	print_colored_text(LIGHTCYAN, "Type \"remove x\" to remove a player from the game (x - the player index).", true);
	print_colored_text(LIGHTCYAN, "Type \"ping\" to detect the player who has the slowest connetion to the server.", true);
	print_colored_text(LIGHTCYAN, "Type \"random\" to get the players list in a random order.", true);

	while(true) {
		cout << endl;
		string input = read_colored_text(LIGHTRED);
		transform(input.begin(), input.end(), input.begin(), ::tolower);

		if(!serverActive) break;

		if(input == "players") {
			string playerList;
			for(LF2Player &p : players) {
				playerList += "Player number " + to_string(p.playerIndex+1) + ": \"" + p.playerName + "\"" + (!p.isActive ? " (disconnected)" : "") + "\n";
			}
			cout << playerList << endl;
			try {
				clip::setClipboardText(playerList);
				cout << "The players list has been copied into your clipboard.\n" <<
				        "Press \"ctrl+v\" in order to paste." << endl;
			}
			catch(clip::clipboard_exception &ce) {
				print_colored_text(YELLOW, "Error: " + string(ce.what()), true);
			}
		}
		else if(input == "random") {
			// obtain a time-based seed:
			unsigned seed = static_cast<unsigned>(system_clock::now().time_since_epoch().count());
			//suffle names
			vector<string> names;
			for(LF2Player &p : players) {
				if(p.isActive) {
					names.emplace_back(p.playerName);
				}
			}
			shuffle (names.begin(), names.end(), default_random_engine(seed));
			string playersShuffledList;
			for(size_t i = 0; i < names.size(); i++) {
				playersShuffledList += names[i] + "\n";
			}
			cout << "\nPlayers by random order:\n" << playersShuffledList << endl;
			try {
				clip::setClipboardText(playersShuffledList);
				cout << "The shuffled players list has been copied into your clipboard.\n" <<
				        "Press \"ctrl+v\" in order to paste." << endl;
			}
			catch(clip::clipboard_exception &ce) {
				print_colored_text(YELLOW, "Error: " + string(ce.what()), true);
			}
		}
		else if(input == "remove" || input.find("remove ") == 0) { //string starts with "remove "
			if(input.length() <= 7) {
				cout << "Need player index." << endl;
				continue;
			}
			else {
				string s_index = input.substr(7);
				stringstream myStream(s_index);
				int index;
				if(myStream >> index && index >= 1 && index <= static_cast<int>(players.size())) {
					LF2Player &p = players[index-1];
					if(!p.isActive) {
						cout << p.playerName << " has already disconnected." << endl;
						continue;
					}
					p.isActive = false;
					cout << p.playerName << " has been removed from the game." << endl;
				}
				else {
					cout << "Invalid player index." << endl;
				}
			}
		}
		else if (input == "ping") {
			if(!gameLoaded) {
				cout << "Wait for game to load, then try again." << endl;
				continue;
			}
			cout << "Analyzing connections, please wait..." << endl;

			for(LF2Player &p : players) {
				p.totalWaitAmount = milliseconds(0);
			}

			pingPlayerIndex = 0;//pingEventFinsihed.Reset();
			pingPlayerLoopsCount = 0;
			pingTotalLoopsCount = 0;
			pingCommandActive = true;

			//wait for data gathrering to complete
			unique_lock<mutex> pm(pingMutex);
			pingCV.wait(pm, []{return (pingPlayerIndex >= players.size() || !serverActive);});

			//check that the game is still running after the sleep
			if(!serverActive) {
				return;
			}

			for(LF2Player &p : players) {
				//Make average
				p.totalWaitAmount = p.totalWaitAmount.load() / ping_command_loops_per_player;

				//FIXME This can't be right...
				//LF2 has a inner clock, and it delays its respond if the request was sent quicker that its FPS ratio.
				//So I subtract this ratio from the total ping result.
				//Apparently LF2 waits 2 frames before it responds.
				//I don't sure why. I will check it again when I have some time.
				p.totalWaitAmount = p.totalWaitAmount.load() - milliseconds(ping_lf2_wait_between_frames * 2); //FIXME No better way to subtract?
				if (p.totalWaitAmount.load() < milliseconds(0)) p.totalWaitAmount = milliseconds(0);
			}

			multimap<unsigned, string> ping_player;
			for(LF2Player &p : players) {
				if(p.isActive) {
					ping_player.insert( pair<unsigned, string>(p.totalWaitAmount.load().count(), p.playerName) );
				}
			}
			cout << "\nDelay times for players, starting from the slowest player:" << endl;
			print_colored_text(LIGHTCYAN, "(Zero indicates that the player does not create lags)\n", true);
			for (auto p = ping_player.rbegin(); p != ping_player.rend(); ++p) {
				cout << p->second << "\t\t - " << p->first << "ms" << endl;
			}
		}
		else {
			cout << "Invalid command." << endl;
		}
	}
}
