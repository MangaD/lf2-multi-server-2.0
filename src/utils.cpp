/**
 * Utility functions implementation
 * 
 * C# code: Ayalx
 * C++ code: MangaD
 * Copyright: Ayalx
 */

#include "utils.hpp"
#include <string>
#include "rlutil.h"

void print_colored_text(int color, std::string text, bool newline) {
	rlutil::saveDefaultColor();
	rlutil::setColor(color);
	std::cout << text;
	if(newline){
		std::cout << std::endl;
	}
	rlutil::resetColor();
}

std::string read_colored_text(int color) {
	rlutil::saveDefaultColor();
	rlutil::setColor(color);
	std::string input;
	getline(std::cin, input);
	rlutil::resetColor();
	return input;
}
